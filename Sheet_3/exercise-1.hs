universalQ :: (a -> Bool) -> [a] -> Bool
universalQ function list = foldr (&&) True (map function list)

map' :: (a -> b) -> [a] -> [b]
map' function list = foldr (\x acc -> function x : acc) [] list

removeDuplicates :: Eq a => [a] -> [a]
removeDuplicates list = foldr (\x acc -> if (filter (==x) acc) /= [] then acc else x : acc) [] list