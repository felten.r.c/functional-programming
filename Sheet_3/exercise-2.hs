data Rose a = Node a [Rose a] deriving Show

zipWithRose :: (a -> b -> c) -> Rose a -> Rose b -> Rose c
zipWithRose function (Node a1 children1) (Node a2 children2) = Node (function a1 a2) (zipWith (zipWithRose function) children1 children2)

mapAndFold :: (a -> b) -> (b -> b -> b) -> Rose a -> b
mapAndFold f g (Node r children) = g (f r) (foldr (g)  (map (mapAndFold f g) children))