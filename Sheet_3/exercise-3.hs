dropMult :: Int -> [Int] -> [Int]
dropMult x xs = [y | y <- xs, y `mod` x /= 0]

dropAll :: [Int] -> [Int]
dropAll (x:xs) = x : dropAll (dropMult x xs)

primes :: [Int]
primes = dropAll [2 ..]

goldbach :: Int -> [(Int, Int)]
goldbach n = [(x, y) | x <- takeWhile (<= n) primes, y <- takeWhile (<= n) primes, x + y == n, x `mod` 2 == 1, y `mod` 2 == 1, x <= y]

range :: [a] -> Int -> Int -> [a]
range (pre:xs) m n = if m > 0 && n > 0 then range xs (m - 1) (n - 1) else if m > 0 then range (xs:post) (m - 1) n else if n > 0 then range (pre:xs) m (n-1) else if m <= 0 && n <=0 then (pre:xs)++ [post] else []